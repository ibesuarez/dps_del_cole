# dps_del_cole

#clonar repo
git clone ssh://fhurtado@web546.webfaction.com/home/fhurtado/webapps/inpaktu/repos/dpsdelcole.git dps_del_cole

#instalar vendor
composer install

#inicializar el env de la app, en Development
#indicar 0 para Development 
php init --env=Production --overwrite=All

#copiar los archivos de config. que estan en los archivos de este repo

/common/config/main.php
/frontend/config/main.php
/backend/config/main.php


#crear la database, dps_del_cole, y migrar
php yii migrate
